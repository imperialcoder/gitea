build:
	sudo -u git ssh-keygen -t rsa -b 4096 -C "Gitea Host Key"
	sudo cp gitea.sh /app/gitea/gitea
	sudo chmod +x /app/gitea/gitea
	docker-compose build
